require('dotenv').config({
    silent:true
})
const express = require('express')
const app = express()
const PORT = process.env.PORT || 3000
const path = require("path")
const api = require('./src/server/api');

(async () => {
    
    await require('./src/server/mongo')(app)

    app.compile = require('./src/server/compile')

    
    app.use('/admin/assets', express.static(path.join(process.cwd(), 'src','admin','public')))
    app.use('/', express.static(path.join(process.cwd(), 'dist')))
    app.use('/admin*', (req, res) => {
        res.sendFile(path.join(process.cwd(), 'src/admin/public/index.html'))
    })
    api(app)
    app.listen(PORT, async () => {
        console.log(`Listening on ${PORT}`)
        await app.compile()
    })


})()