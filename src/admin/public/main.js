import Vue from 'https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.esm.browser.js'
import funql from 'https://cdn.jsdelivr.net/npm/funql-api@1.2.6/client.js'
    
const fql = funql('http://localhost:3000')

new Vue({
    el:'#app',
    data(){
        return {
            section:'',
            pages:[],
            dirty:false
        }
    },
    async created(){
        this.pages = await fql('loadFixtures')
    },
    mounted(){
        document.addEventListener("keyup", e=>{
            if(e.key==='Escape'){
                this.section=''
            }
        });
    },
    computed:{
        aboutMessage(){
            return sectionValue('about','about_message')
        }
    },
    methods:{
        reloadPreview(){
            document.querySelector('iframe').src = document.querySelector('iframe').src
        },
        blur(){
            this.section=''
            return true;
        },
        onChange(){
            this.dirty=true
        },
        async save(){
            await fql('savePages', [this.pages.map(p=>({...p}))])
            this.dirty=false
            this.reloadPreview()
            this.blur()
        },
        sectionValue(page, section){
            return this.pages.find(p=>p.name==page) && this.pages.find(p=>p.name==page).sections.find(s=>s.name==section).value
        },
        onSectionTextUpdate(e,page,section){
            this.pages.find(p=>p.name==page).sections.find(s=>s.name==section).value = e.target.value
            return true
        },
        goto(name){
            this.section = name
        }
    }
})
