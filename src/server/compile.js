var builder = require('./lib/builder');

let mongoose = require('mongoose')
var m = n => mongoose.model(n)



var config = {
	metaTitle: '',
	metaTitlePrefix: 'Bon Beau en Bauges'
};

module.exports =async function compile(){

	let pagesDocs = (await m('Page').find({})).map(p=>p.toJSON())

	let pages = {}
	pagesDocs.forEach(p=>{
		let sections = p.sections
		p.sections = {}
		sections.forEach(s=>p.sections[s.name]=s)
		pages[p.name]=p
	})

	await builder.transformFile({
		target: '/index.html',
		source: "src/templates/home.pug",
		mode: 'pug',
		context: {
			...config,
			pages
		}
	});

	await builder.transformFile({
		target: '/a-propos/index.html',
		source: "src/templates/about-us.pug",
		mode: 'pug',
		context: {
			...config,
			pages
		}
	});

	await builder.transformFile({
		target: '/contact/index.html',
		source: "src/templates/contact.pug",
		mode: 'pug',
		context: {
			...config,
			pages
		}
	});


	var db = require('./lib/database').get(builder);
	var blogArticles = await db.getItems({
		source: 'blog-articles'
	});

	await builder.transformFile({
		target: '/blog/index.html',
		source: "src/templates/blog.pug",
		mode: 'pug',
		context: {
			...config,
			items: blogArticles,
			pages
		}
	});


	await Promise.all(blogArticles.map(article => {
		return (async() => {
			await builder.transformFile({
				target: `${article.publicUrl||'/'+article.name}/index.html`,
				source: 'src/templates/blog-article.pug',
				mode: 'pug',
				context: {
					...config,
					item: article,
					pages
				}
			});
		})();
	}));


	var actorsItems = await db.getItems({
		source: 'actors'
	})
	await builder.transformFile({
		target: '/acteurs-locaux/index.html',
		source: "src/templates/local-entrepreneurs.pug",
		mode: 'pug',
		context: {
			pages,
			...config,
			bon: actorsItems.filter(a=>a.type==='bon'),
			beau: actorsItems.filter(a=>a.type==='beau'),
			actus: actorsItems.filter(a=>a.type==='actus'),
			partager: actorsItems.filter(a=>a.type==='partager')
		}
	});
	
	
	
	await Promise.all(actorsItems.map(actor => {
		return (async() => {
			await builder.transformFile({
				target: `${actor.publicUrl||'/'+actor.name}/index.html`,
				source: 'src/templates/actor-details.pug',
				mode: 'pug',
				context: {
					...config,
					item: actor,
					relatedArticles: blogArticles.filter(article=> (actor.relatedArticles||[]).includes(article.name)),
					relatedActors: actorsItems.filter(curr=>{
						return curr.fsId != actor.fsId && curr.type == actor.type
					}),
					categoryTitle:{
						bon: 'Epicerie, alimentation, producteurs locaux'
					},
					pages
				}
			});
		})();
	}));



	await builder.transformFile({
		target: '/css/styles.css',
		source: "src/styles.css",
		mode: 'scss'
	})

	/*
		await builder.transformFile({
			target: '/js/common.js',
			source: "src/js/common.js",
			mode: 'js'
		})
	*/
	
	await builder.copy('src/public','dist')
	
	console.log('builder done')

}