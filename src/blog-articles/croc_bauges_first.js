module.exports = {
    "metaTitle":"Sample Blog Article",
    "fsId": "5c5786250513e20085a33f10",
    "name": "epicerie-bio-croq-bauges",
    "title": "Croc'Bauges",
    mainImage: "/epicerie-bio-croq-bauges/croc-bauges.JPG",
    thumbImage:"https://p8.storage.canalblog.com/83/68/1260698/100052038.jpg",
    "shortDescription": "Depuis sa naissance en 2009, l’épicerie Croc'Bauges a bien grandit et fait aujourd’hui partie du paysage.",
    "created_at": 1554411601371,
    "author":"Celine Aubert",
    "published": true,
    "order": "0"
};